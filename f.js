const Discord = require('discord.js');
let { colors } = require('./config.json');

module.exports = {
  formatTime: function (timeInSeconds) {
    const timeLeft = timeInSeconds
    let hours = Math.floor(timeLeft / 3600);
    let r1 = timeLeft % 3600;
    let minutes = Math.floor(r1 / 60);
    let seconds = Math.floor(r1 % 60);

    let finalTime;

    if (hours !== 0 && minutes !== 0 && seconds !== 0) {
      finalTime = `${hours} hour(s), ${minutes} minute(s) and ${seconds} second(s)`;
    } else if (hours !== 0 && minutes !== 0 && seconds === 0) {
      finalTime = `${hours} hour(s) and ${minutes} minute(s)`;
    } else if (hours !== 0 && minutes === 0 && seconds === 0) {
      finalTime = `${hours} hour(s)`;
    } else if (hours !== 0 && minutes === 0 && seconds !== 0) {
      finalTime = `${hours} hour(s) and ${seconds} second(s)`;
    } else if (hours === 0 && minutes !== 0 && seconds !== 0) {
      finalTime = `${minutes} minute(s) and ${seconds} second(s)`;
    } else if (hours === 0 && minutes !== 0 && seconds === 0) {
      finalTime = `${minutes} minute(s)`;
    } else if (hours === 0 && minutes === 0 && seconds === 0) {
      finalTime = `${seconds} second(s)`;
    } else if (hours === 0 && minutes === 0 && seconds !== 0) {
      finalTime = `${seconds} second(s)`;
    } else {
      finalTime = "`You should not see this message, contact staff ASAP!!!`";
    }

    return finalTime;
  },
  sendEmb: function (channel, color, text) {
    if (!channel || !color || !text) return new Error('Some parameters were not provided!');

    if (!text) return new Error('No text provided.');

    let emb = new Discord.MessageEmbed()
      .setDescription(text);

    if (color === 'green') {
      emb.setColor(colors.green);
    } else if (color === 'red') {
      emb.setColor(colors.red);
    } else if (color === 'orange') {
      emb.setColor(colors.orange);
    } else if (color === 'blue') {
      emb.setColor(colors.blue);
    } else return new Error('Invalid type.');


    return channel.send(emb);
  },
  getIDfromMention: function (a) {
    if (typeof a !== typeof "string")
      return new Error("Param 1 is not a string!");

    if (a.startsWith("<@") && a.endsWith(">")) {
      let id = a.slice(2, -1);
      if (id.startsWith("!")) {
        id = id.slice(1);
      }
      return id; //String (Discord ID)
    } else return null;
  }
};