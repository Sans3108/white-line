console.log('Starting up... It will take a while');
require('dotenv').config();

const Discord = require('discord.js');
const fs = require('fs');

const db = require('quick.db');

if (!db.fetch('guilds')) {
  db.set('guilds', []);
}

const botConfig = require('./config.json');
const f = require('./f.js');

const DisTube = require('distube');

let bot = new Discord.Client({ fetchAllMembers: true, restTimeOffset: 100 });

const distube = new DisTube(bot, { leaveOnStop: true, searchSongs: false, leaveOnEmpty: true, leaveOnFinish: true, updateYouTubeDL: true });

bot.config = botConfig;
bot.f = f;
bot.commands = new Discord.Collection();
bot.skips = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  bot.commands.set(command.name, command);
}

distube
  .on('playSong', async (msg, queue, song) => {
    try {
      msg.reactions.cache.find(i => i.emoji.id === '825148307286130748').users.remove(bot.user.id);
    } catch (e) {
      console.error(e);
    }

    if (song.isLive === true) {
      bot.f.sendEmb(msg.channel, 'red', `Skipping \`${song.name}\` because it was a livestream!`);
      distube.skip(msg);
      if (bot.skips.has(msg.guild.id)) {
        bot.skips.get(msg.guild.id).skips = 0;
        bot.skips.get(msg.guild.id).skippers = [];
      }
      return;
    }

    if (song.duration > 10800) {
      bot.f.sendEmb(msg.channel, 'red', `Skipping \`${song.name}\` because it is longer than 3h! (Song length: ${song.formattedDuration})`);
      distube.skip(msg);
      if (bot.skips.has(msg.guild.id)) {
        bot.skips.get(msg.guild.id).skips = 0;
        bot.skips.get(msg.guild.id).skippers = [];
      }
      return;
    }

    let emb = new Discord.MessageEmbed().setColor(bot.config.colors.green).setTitle(song.name).setURL(song.url).setAuthor('Now Playing:', bot.user.avatarURL()).setDescription(`**Duration:** ${song.formattedDuration}\n**Channel:** ${song.info.videoDetails.author.name}\n\n**Requested by:** ${song.user}`);

    try {
      emb.setThumbnail(song.thumbnail);
    } catch {
      console.error('Thumbnail error.');
    }

    if (bot.skips.has(msg.guild.id)) {
      bot.skips.get(msg.guild.id).skips = 0;
      bot.skips.get(msg.guild.id).skippers = [];
    }

    msg.channel.send(emb);
  })
  .on('addSong', async (msg, queue, song) => {
    try {
      msg.reactions.cache.find(i => i.emoji.id === '825148307286130748').users.remove(bot.user.id);
    } catch (e) {
      console.error(e);
    }

    let q = queue.songs.map((song, index) => ({ idx: index, sid: song.id }));
    let sPos = q.find(i => i.sid === song.id).idx;

    let emb = new Discord.MessageEmbed().setColor(bot.config.colors.blue).setTitle(song.name).setURL(song.url).setAuthor('Added to Queue:', bot.user.avatarURL()).setDescription(`**Duration:** ${song.formattedDuration}\n**Channel:** ${song.info.videoDetails.author.name}\n**Position in Queue:** ${sPos}\n\n**Requested by:** ${song.user}`);
    try {
      emb.setThumbnail(song.thumbnail);
    } catch {
      console.error('Thumbnail error.');
    }
    msg.channel.send(emb);
  })
  .on('playList', (msg, queue, playlist, song) => {
    try {
      msg.reactions.cache.find(i => i.emoji.id === '825148307286130748').users.remove(bot.user.id);
    } catch (e) {
      console.error(e);
    }

    let emb = new Discord.MessageEmbed()
      .setColor(bot.config.colors.green)
      .setTitle(song.name)
      .setURL(song.url)
      .setAuthor('Now Playing:', bot.user.avatarURL())
      .setDescription(`**Duration:** ${song.formattedDuration}\n**Channel:** ${song.info.videoDetails.author.name}\n\n**Requested by:** ${song.user}\n\n_(Rest of ${playlist.songs.length - 1} songs have been added to queue)_`);
    try {
      emb.setThumbnail(song.thumbnail);
    } catch {
      console.error('Thumbnail error.');
    }
    msg.channel.send(emb);
  })
  .on('addList', (msg, queue, playlist) => {
    try {
      msg.reactions.cache.find(i => i.emoji.id === '825148307286130748').users.remove(bot.user.id);
    } catch (e) {
      console.error(e);
    }

    let emb = new Discord.MessageEmbed().setColor(bot.config.colors.blue).setTitle('Added playlist to queue').setDescription(`${playlist.songs.length} songs have been added to queue`);
    msg.channel.send(emb);
  })
  .on('initQueue', queue => {
    queue.autoplay = false;
    queue.volume = 100;
  })
  .on('error', (msg, e) => {
    console.error(e);
    bot.f.sendEmb(msg.channel, 'red', 'An error occurred.');
  });

bot.on('ready', async () => {
  console.log('Ready!');

  function setPresence() {
    if (bot.user.id === '824635130249084960') {
      bot.user.setActivity('music with you', { type: 'LISTENING' });
    } else {
      bot.user.setActivity('myself evolving', { type: 'WATCHING' });
    }
  }
  setInterval(setPresence, 1800000);
  setPresence();
});

bot.on('voiceStateUpdate', async (oldState, newState) => {
  if (newState.guild.me.voice.channel && !newState.guild.me.voice.serverDeaf) {
    newState.guild.me.voice.setDeaf(true).catch(console.error);
  }
});

const cooldowns = new Discord.Collection();

bot.on('message', async message => {
  if (!message.channel.guild) return;
  if (message.channel.guild && !message.guild.me.hasPermission('ADMINISTRATOR')) return;

  if (message.content === '```xl\nPromise { <pending> }\n```' && message.author.id === bot.user.id) {
    message.delete();
  }
  if (message.content === '```xl\nPromise { undefined }\n```' && message.author.id === bot.user.id) {
    message.delete();
  }

  if (!db.fetch('guilds').find(i => message.guild.id === i.id)) {
    let guilds = db.fetch('guilds');
    guilds.push({ id: message.guild.id, prefix: bot.config.prefix });
    db.set('guilds', guilds);
  }

  if (bot.f.getIDfromMention(message.content) === bot.user.id) return bot.f.sendEmb(message.channel, 'blue', `Hey! My prefix in this server is: \`${db.fetch('guilds').find(i => message.guild.id === i.id).prefix}\``);

  if (message.author.bot || !message.content.startsWith(db.fetch('guilds').find(i => message.guild.id === i.id).prefix)) return;

  const args = message.content.slice(db.fetch('guilds').find(i => message.guild.id === i.id).prefix.length).split(/ +/);
  const commandName = args.shift().toLowerCase();

  const command = bot.commands.get(commandName) || bot.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
  if (!command) return;

  // Dev only commands
  if (command.ownerOnly && message.author.id !== bot.config.ownerID) return;
  // Guild only commands - disabled
  //if (command.guildOnly && message.channel.type !== 'text') return;
  // Permissions check
  if (message.guild && command.permissions[0] && !message.member.hasPermission(command.permissions)) return bot.f.sendEmb(message.channel, 'red', `You are missing the permissions to run this command!\nPermissions needed: \`${command.permissions.join(', ')}\``);

  // Cooldowns
  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Discord.Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 3) * 1000;

  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;

      let cooldownEmbed = new Discord.MessageEmbed().setColor(bot.config.color.red).setDescription(`(Cooldown) Please wait ${bot.f.formatTime(timeLeft)} before reusing the \`${command.name}\` command, ${message.author}!`);

      return message.channel.send(cooldownEmbed);
    }
  }

  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  // Command args
  if (command.args && !args.length) {
    let noArgsEmb = new Discord.MessageEmbed().setColor(bot.config.colors.red);

    let reply = `You didn't provide any arguments, ${message.author}!`;

    if (command.usage) {
      reply += `\nThe proper usage would be: \`${db.fetch('guilds').find(i => message.guild.id === i.id).prefix}${command.name} ${command.usage}\``;
    }

    noArgsEmb.setDescription(reply);

    return message.channel.send(noArgsEmb);
  }

  // Command execution
  try {
    command.execute(message, args, bot, distube);
  } catch (error) {
    console.error(error);

    let errEmb = new Discord.MessageEmbed().setColor(bot.config.color.red).setTitle(`Well this wasn't supposed to happen...`).addField(`There was an error when executing the command!`, `\`${error.message}\``);

    message.channel.send(errEmb);
  }
});

bot.login(process.env.token);
