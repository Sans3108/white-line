function clean(text) {
  if (typeof text === 'string') {
    return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
  } else return text;
}

const Discord = require('discord.js');
const fs = require('fs');
const db = require('quick.db');

const splitter = '-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=-+=';

module.exports = {
  name: 'eval',
  description: 'Evaluates the given code and outputs the result.',
  detailedDescription: `This command can be used to evaluate JavaScript code. It has top level \`await\` and supports Discord JavaScript code blocks.`,
  examples: function (pref) {
    return [`${pref}${this.name} console.log('Hello World!');`, `${pref}${this.name} \`\`\`js\nmessage.channel.send(await keyv.get('config').prefix);\n\`\`\``];
  },
  usage: '<JS code>',
  aliases: ['evaluate'],
  permissions: [],
  group: 'dev',
  cooldown: 3,
  args: false,
  ownerOnly: true,
  guildOnly: false,
  execute: async (message, args, bot, distube) => {
    try {
      let code = args.join(' ');
      if (code.startsWith('```js') && code.endsWith('```')) {
        code = code.slice(5, -3);
      }

      let evaled = eval(`;(async () => {${code}})();`);

      if (typeof evaled !== 'string') evaled = require('util').inspect(evaled);

      message.channel.send(clean(evaled), { code: 'xl' });
    } catch (err) {
      console.log(err);
      bot.f.sendEmb(message.channel, 'red', `\`ERROR\` \`\`\`xl\n${clean(err.message)}\n\`\`\``);
    }
  },
};
