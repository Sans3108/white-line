const Discord = require('discord.js');
const db = require('quick.db');
const _ = require('lodash');
const prompter = require('discordjs-prompter');

module.exports = {
  name: 'remove',
  description: 'Remove a song from the queue!',
  detailedDescription: "Remove a song from the queue. The index of the song refers to the position of the song in the queue. You can remove a song if you've added it, if you're an admin or if you're a DJ. Being alone with the bot also works.",
  examples: function (pref) {
    return [`${pref}${this.name} 6`, `${pref}${this.name} 9`];
    // example: [`${pref}${this.name}`];
  },
  usage: '[song index]', // [required] <optional>
  aliases: ['rm'],
  permissions: [],
  group: 'music',
  cooldown: 1,
  args: true,
  ownerOnly: false,
  guildOnly: true,
  execute: async function (message, args, bot, distube) {
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', 'No music is being played!');

    let rmList = queue.songs.slice(1).map((song, index) => ({ idx: index + 1, author: song.user.id, sid: song.id, name: song.name, url: song.url }));

    if (parseInt(args[0]) > 0) {
      let songIndex = parseInt(args[0]);

      let removed = rmList.find(i => i.idx === songIndex);
      if (!removed) return bot.f.sendEmb(message.channel, 'red', 'Invalid song index!');
      let index = queue.songs.indexOf(queue.songs.find(i => i.id === removed.sid));

      if (message.member.roles.cache.map(i => i.name.toLowerCase()).includes('dj') || message.member.permissions.has('ADMINISTRATOR') || message.guild.me.voice.channel.members.size === 2 || message.author.id === removed.author) {
        queue.songs.splice(index, 1);

        bot.f.sendEmb(message.channel, 'green', `Removed \[ [__${removed.name}__](${removed.url}) \] from the queue!`);
      }
    } else return bot.f.sendEmb(message.channel, 'red', 'Invalid song index!');
  },
};
