const Discord = require("discord.js");
const db = require('quick.db');
const _ = require('lodash');
const prompter = require('discordjs-prompter');

module.exports = {
  name: "shuffle",
  description: "Shuffle the queue!",
  detailedDescription: "Shuffle the queue. Randomly changes the positions of all the songs currently in the queue.",
  examples: function (pref) {
    return [`${pref}${this.name}`]; 
    // example: [`${pref}${this.name}`];
  },
  usage: "", // [required] <optional>
  aliases: ['sh'],
  permissions: [],
  group: "music",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async function (message, args, bot, distube) {
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', "No music is being played!");

    if (
      message.member.roles.cache.map(i => i.name.toLowerCase()).includes('dj') ||
      message.member.permissions.has('ADMINISTRATOR') ||
      message.guild.me.voice.channel.members.size === 2
    ) {
      distube.shuffle(message);
      bot.f.sendEmb(message.channel, 'green', `Shuffled the queue!`);
    } else bot.f.sendEmb(message.channel, 'red', `You need to be an Administrator, DJ or be alone with the bot for this command to work!`);
  }
};