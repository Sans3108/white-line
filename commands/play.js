const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "play",
  description: "Play a song!",
  detailedDescription: "Play a song! You can provide a YouTube video/playlist url to be played or some text which will be used to search for songs on YouTube, the first result will be played.",
  examples: function (pref) {
    return [`${pref}${this.name} Geoxor - Aether`, `${pref}${this.name} https://youtube.com/playlist?list=PLRBp0Fe2GpgmsW46rJyudVFlY6IYjFBIK`, `${pref}${this.name} https://youtu.be/yJg-Y5byMMw`];
    // example: [`${pref}${this.name}`];
  },
  usage: "[YT song/playlist url | search query]", // [required] <optional>
  aliases: ['p'],
  permissions: [],
  group: "music",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    if (!message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in a Voice Channel!");
    if (message.guild.me.voice.channel && message.guild.me.voice.channel !== message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in the same Voice Channel as me!");
    if (!args[0]) return bot.f.sendEmb(message.channel, 'red', "Specify what do you want to play!");

    let searchString = args.join(' ');

    if (searchString.startsWith('http://') || searchString.startsWith('https://')) {
      if (searchString.startsWith('https://youtube.com') || searchString.startsWith('https://youtu.be') || searchString.startsWith('https://www.youtube.com') || searchString.startsWith('https://www.youtu.be')) {
        // nothing lol
      } else return bot.f.sendEmb(message.channel, 'red', "I can only play YouTube links!");
    }
    
    message.react('825148307286130748');

    await distube.play(message, searchString);
  }
};
