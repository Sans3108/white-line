const Discord = require('discord.js');
const db = require('quick.db');
const _ = require('lodash');
const prompter = require('discordjs-prompter');

module.exports = {
  name: 'loop',
  description: 'Control the looping mode!',
  detailedDescription: 'Control how the queue loops. Using this command without arguments will toggle between full queue loop and no loop. However using an argument you can specify the looping mode:\n"song" is for repeating the current song\n"queue" is for repeating the whole queue\n"off" will turn off looping\n\nAdditionally if the new looping mode is set to the current mode the looping will be disabled, aka turned off.',
  examples: function (pref) {
    return [`${pref}${this.name}`, `${pref}${this.name} queue`, `${pref}${this.name} song`, `${pref}${this.name} off`];
    // example: [`${pref}${this.name}`];
  },
  usage: '<queue | song | off>',
  aliases: ['l', 'lp'],
  permissions: [],
  group: 'music',
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async function (message, args, bot, distube) {
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', 'No music is being played!');

    if (message.member.roles.cache.map(i => i.name.toLowerCase()).includes('dj') || message.member.permissions.has('ADMINISTRATOR') || message.guild.me.voice.channel.members.size === 2) {
      let currentMode = queue.repeatMode;
      if (!args[0]) {
        if (currentMode === 0) distube.setRepeatMode(message, 2);
        if (currentMode === 1 || currentMode === 2) distube.setRepeatMode(message, 0);
      } else {
        if (['song', 's', '1'].includes(args[0].toLowerCase())) {
          distube.setRepeatMode(message, 1);
        } else if (['queue', 'q', '2'].includes(args[0].toLowerCase())) {
          distube.setRepeatMode(message, 2);
        } else if (['off', 'o', '0'].includes(args[0].toLowerCase())) {
          distube.setRepeatMode(message, 0);
        } else {
          if (currentMode === 0) distube.setRepeatMode(message, 2);
          if (currentMode === 1 || currentMode === 2) distube.setRepeatMode(message, 0);
        }
      }

      let newMode = distube.getQueue(message);
      newMode = newMode.repeatMode;

      let text = newMode ? (newMode == 2 ? 'Looping mode set to `queue`. All songs will be looped trough.' : 'Looping mode set to `song`. This song will be repeated.') : 'Looping mode set to `off`. No songs will be repeated.';

      bot.f.sendEmb(message.channel, 'green', `${text}`);
    }
  },
};
