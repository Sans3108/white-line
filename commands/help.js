const Discord = require('discord.js');
const db = require('quick.db');

module.exports = {
  name: 'help',
  description: 'Lists all my commands or info about a specific command.',
  detailedDescription: `This command can be used to see what commands you have access to, or information about a specific command, such as usage, aliases etc.`,
  examples: function (pref) {
    return [`${pref}${this.name}`, `${pref}${this.name} ping`];
  },
  usage: '<command name>',
  aliases: ['h', 'commands', 'cmds'],
  permissions: [],
  group: 'general',
  cooldown: 3,
  args: false,
  ownerOnly: false,
  guildOnly: false,
  execute: async function (message, args, bot, distube) {
    const data = [];
    const { commands } = message.client;

    let pfix = db.fetch('guilds').find(i => message.guild.id === i.id).prefix;

    if (!args.length) {
      const helpEmbed = new Discord.MessageEmbed().setTitle("**Here's a list of all my commands:**").setThumbnail(bot.user.avatarURL());
      helpEmbed.setColor(bot.config.color.blue);
      helpEmbed.setFooter(`You can send "${pfix}${this.name} [command name]" to get info on a specific command!`);

      const groups = ['general', 'music'];
      groups.forEach(item => {
        let group = commands
          .filter(c => c.group === item)
          .filter(c => {
            if (message.member.hasPermission(c.permissions)) return true;
            else return false;
          })
          .map(command => '_`' + pfix + command.name + '`_' + ` - ${command.description}`)
          .join('\n');

        if (group) {
          helpEmbed.addField(`**${item.charAt(0).toUpperCase() + item.slice(1)} commands:**`, group);
        }
      });

      return message.channel.send(helpEmbed);
    }

    const name = args[0].toLowerCase();
    const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

    if (!command) {
      let invalidCommandEmbed = new Discord.MessageEmbed().setColor(bot.config.color.red).setDescription(`${message.author}, that command doesn't exist!`);

      return message.channel.send(invalidCommandEmbed);
    }

    if (command.ownerOnly && message.author.id !== bot.config.ownerID) return bot.f.sendEmb(message.channel, 'red', `You don't have access to this command!`);
    if (!message.member.hasPermission(command.permissions)) return bot.f.sendEmb(message.channel, 'red', `You don't have access to this command!`);

    const helpEmbed2 = new Discord.MessageEmbed().setTitle(`${pfix}${command.name}`).setColor(bot.config.color.blue).setThumbnail(bot.user.avatarURL());

    if (command.detailedDescription) helpEmbed2.setDescription(`_${command.detailedDescription}_`);
    if (command.aliases[0]) helpEmbed2.addField(`**Aliases:**`, `${command.aliases.join(', ')}`);

    let parameters = `_Arguments surrounded by \`< >\` are optional while arguments surrounded by \`[ ]\` are mandatory. Sometimes an argument may have a separator (\`|\`) which means you are to choose one of the options shown inside the pointed or square brackets._`;
    if (command.usage) {
      helpEmbed2.addField(`**Usage:**`, `${pfix}${command.name} ${command.usage}\n\n${parameters}`);
    } else {
      helpEmbed2.addField(`**Usage:**`, `${pfix}${command.name}`);
    }

    const commandCooldown = command.cooldown || 3;

    helpEmbed2.addField(`**Cooldown:**`, `${bot.f.formatTime(commandCooldown)}`);
    helpEmbed2.addField(`** **`, `** **`);

    if (command.examples) helpEmbed2.addField(`**Examples:**`, `${command.examples(pfix).join('\n')}`);

    if (command.permissions[0]) {
      helpEmbed2.addField(`** **`, `** **`);
      helpEmbed2.addField(`**Permissions Needed:**`, `\`${command.permissions.join('`, `')}\``);
    }

    message.channel.send(helpEmbed2);
  },
};
