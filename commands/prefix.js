const Discord = require('discord.js');
const db = require('quick.db');

module.exports = {
  name: 'prefix',
  description: 'Change the prefix on this server!',
  detailedDescription: `Change the prefix of the bot on your server.`,
  examples: function (pref) {
    return [`${pref}${this.name} ?`, `${pref}${this.name} !`];
    // example: [`${pref}${this.name}`];
  },
  usage: '[new prefix]', // [required] <optional>
  aliases: [],
  permissions: ['MANAGE_GUILD', 'MANAGE_MESSAGES'],
  group: 'general',
  cooldown: 3,
  args: true,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    let guilds = db.fetch('guilds');
    guilds.find(i => i.id === message.guild.id).prefix = args[0];
    db.set('guilds', guilds);

    bot.f.sendEmb(message.channel, 'green', `Changed my prefix to ${db.fetch('guilds').find(i => i.id === message.guild.id).prefix}`);
  },
};
