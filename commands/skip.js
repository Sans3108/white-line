const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "skip",
  description: "Skip a song!",
  detailedDescription: "Skip a song that is currently playing. You can only skip a song if you're a DJ, Administrator or if you're alone with the bot, otherwise your skip request will be added to a voting pool, if enough people run this command the song will be skipped.",
  examples: function (pref) {
    return [`${pref}${this.name}`]; 
    // example: [`${pref}${this.name}`];
  },
  usage: "", // [required] <optional>
  aliases: ['s'],
  permissions: [],
  group: "music",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    if (!message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in a Voice Channel!");
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', "No music is being played!");
    if (message.guild.me.voice.channel && message.guild.me.voice.channel !== message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in the same Voice Channel as me!");

    if (!bot.skips.has(message.guild.id)) {
      bot.skips.set(message.guild.id, { skips: 0, skippers: [] });
    }

    if (message.member.roles.cache.filter(i => i.id !== message.guild.id).map(i => i.name.toLowerCase()).includes('dj') || message.member.permissions.has('ADMINISTRATOR') || message.guild.me.voice.channel.members.size === 2) {
      bot.f.sendEmb(message.channel, 'green', `Skipping...`);
      bot.skips.get(message.guild.id).skips = 0;
      bot.skips.get(message.guild.id).skippers = [];
      distube.skip(message);
      return;
    }

    let count = message.guild.me.voice.channel.members.size - 1;

    let reqSkips;

    if (count >= 2 && count <= 4) {
      reqSkips = count;
    } else if (count >= 5 && count <= 10) {
      reqSkips = Math.ceil(count - (count / 4));
    } else {
      reqSkips = Math.ceil(count / 2);
    }

    if (bot.skips.get(message.guild.id).skippers.includes(message.author.id)) {
      if (bot.skips.get(message.guild.id).skips >= reqSkips) {
        bot.skips.get(message.guild.id).skips = 0;
        bot.skips.get(message.guild.id).skippers = [];

        bot.f.sendEmb(message.channel, 'green', `Skipping...`);
        distube.skip(message);
        return;
      } else {
        bot.f.sendEmb(message.channel, 'red', `You already voted to skip this song, ${message.author}!`);
        return;
      }
    }

    bot.skips.get(message.guild.id).skips += 1;
    bot.skips.get(message.guild.id).skippers.push(message.author.id);

    if (bot.skips.get(message.guild.id).skips >= reqSkips) {
      bot.skips.get(message.guild.id).skips = 0;
      bot.skips.get(message.guild.id).skippers = [];

      bot.f.sendEmb(message.channel, 'green', `Skipping...`);
      distube.skip(message);
    } else {
      bot.f.sendEmb(message.channel, 'blue', `Skipping? ${bot.skips.get(message.guild.id).skips}/${reqSkips}`);
    }
  }
};