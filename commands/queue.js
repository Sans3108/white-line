const Discord = require('discord.js');
const db = require('quick.db');
const _ = require('lodash');
const prompter = require('discordjs-prompter');

module.exports = {
  name: 'queue',
  description: 'Check the current queue!',
  detailedDescription: 'Displays the queue in order.',
  examples: function (pref) {
    return [`${pref}${this.name}`];
    // example: [`${pref}${this.name}`];
  },
  usage: '', // [required] <optional>
  aliases: ['q'],
  permissions: [],
  group: 'music',
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', 'No music is being played!');
    let songs = queue.songs;

    let np = `\[ [__${songs[0].name}__](${songs[0].url}) \] - ${songs[0].formattedDuration} | ${songs[0].user}`;
    let songList = songs.slice(1).map((song, index) => `**${index + 1}.** \[ [__${song.name}__](${song.url}) \] - ${song.formattedDuration} | ${song.user}`);

    let repeatMode = queue.repeatMode;
    let ql = repeatMode ? (repeatMode == 2 ? { txt: '🔁', text: '(queue)' } : { txt: '🔂', text: '(song)' }) : { txt: 'Off', text: '' };

    if (!songList[10]) {
      let spc;
      if (!songList[0]) {
        spc = '_Nothing..._';
      } else spc = songList.join('\n\n');
      let nothingEmb = new Discord.MessageEmbed()
        .setColor(bot.config.colors.blue)
        .setTitle(`__Queue for **${message.guild.name}**__`)
        .setThumbnail(message.guild.iconURL({ dynamic: true }))
        .setDescription(`**Now Playing:**\n${np}\n\n\n**Next in queue:**\n${spc}\n\n_Loop: ${ql.txt} ${ql.text}_`)
        .setFooter(`Length ${queue.formattedDuration} | ${songs.length} songs`, bot.user.avatarURL({ dynamic: true }));

      message.channel.send(nothingEmb);
    } else {
      let chunks = _.chunk(songList, 10);
      let pages = [];
      let totalPages = chunks.length;
      let count = 0;

      chunks.forEach(chunk => {
        let emb = new Discord.MessageEmbed()
          .setColor(bot.config.colors.blue)
          .setThumbnail(message.guild.iconURL({ dynamic: true }))
          .setTitle(`__Queue for **${message.guild.name}**__`)
          .setDescription(`**Now Playing:**\n${np}\n\n\n**Next in queue:**\n${chunk.join('\n\n')}\n\n_Loop: ${ql.txt} ${ql.text}_`);

        count = count + 1;
        emb.setFooter(`Page: ${count}/${totalPages} | Length ${queue.formattedDuration} | ${songs.length} songs`, bot.user.avatarURL({ dynamic: true }));
        pages.push({ count: count, emb: emb });
      });

      let pageEmb = new Discord.MessageEmbed()
        .setColor(bot.config.colors.blue)
        .setThumbnail(message.guild.iconURL({ dynamic: true }))
        .setDescription(`Just a sec...`);

      let pageMessage = await message.channel.send(pageEmb);

      const filter = (reaction, reactionAuthor) => {
        if (reaction.emoji.name === '⏪' || reaction.emoji.name === '◀️' || reaction.emoji.name === '▶️' || reaction.emoji.name === '⏩' || reaction.emoji.name === '↪️') {
          if (message.author.id === reactionAuthor.id) {
            return true;
          } else return false;
        } else return false;
      };

      let currentPage = 1;

      // ⏪ ◀️ ▶️ ⏩ queue controls
      if (totalPages > 1) {
        await pageMessage.react('⏪');
        await pageMessage.react('◀️');
        await pageMessage.react('▶️');
        await pageMessage.react('⏩');
        await pageMessage.react('↪️');
      } else {
        pages[0].emb.footer = null;
      }

      await pageMessage.edit('', { embed: pages[0].emb });
      if (totalPages > 1) {
        const collector = pageMessage.createReactionCollector(filter, { time: 120000 });

        collector.on('collect', async (reaction, user) => {
          reaction.users.remove(user);

          if (reaction.emoji.name === '▶️') {
            if (currentPage < totalPages) {
              currentPage = currentPage + 1;

              let page = pages.find(page => page.count === currentPage);

              pageMessage.edit('', { embed: page.emb });
            }
          } else if (reaction.emoji.name === '◀️') {
            if (currentPage > 1) {
              currentPage = currentPage - 1;

              let page = pages.find(page => page.count === currentPage);

              pageMessage.edit('', { embed: page.emb });
            }
          } else if (reaction.emoji.name === '⏪') {
            if (currentPage > 1) {
              currentPage = 1;

              let page = pages.find(page => page.count === currentPage);

              pageMessage.edit('', { embed: page.emb });
            }
          } else if (reaction.emoji.name === '⏩') {
            if (currentPage < totalPages) {
              currentPage = totalPages;

              let page = pages.find(page => page.count === currentPage);

              pageMessage.edit('', { embed: page.emb });
            }
          } else if (reaction.emoji.name === '↪️') {
            // page selection
            let q1 = new Discord.MessageEmbed().setTitle('**Jump to page**').setColor(bot.config.colors.orange).setDescription(`What page would you like to jump to?`);

            await prompter
              .message(message.channel, {
                question: q1,
                userId: user.id,
                max: 1,
                timeout: 15000,
              })
              .then(async responses => {
                // If no responses, the time ran out
                if (responses.size) {
                  // Gets the first message in the collection & deletes the response from the author
                  responses.first().delete();
                  let response = responses.first().content;

                  response = parseInt(response) || currentPage;

                  if (response < 1 || response > totalPages) {
                    response = 1;
                  }

                  if (pages.find(page => page.count === parseInt(response))) {
                    currentPage = parseInt(response);
                    let page = pages.find(page => page.count === currentPage);
                    pageMessage.edit('', { embed: page.emb });
                  }
                }
              });
          }
        });

        collector.on('end', collected => {
          pageMessage.reactions.removeAll();
        });
      }
    }
  },
};
