const Discord = require('discord.js');
const _ = require('lodash');
const prompter = require('discordjs-prompter');

module.exports = {
  name: 'now-playing',
  description: 'Check what song is currently playing!',
  detailedDescription: 'Displays the song that is currently playing as well as some information about it.',
  examples: function (pref) {
    return [`${pref}${this.name}`];
    // example: [`${pref}${this.name}`];
  },
  usage: '', // [required] <optional>
  aliases: ['np'],
  permissions: [],
  group: 'music',
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', 'No music is being played!');
    let song = queue.songs[0];

    let emb = new Discord.MessageEmbed()
      .setColor(bot.config.colors.blue)
      .setTitle(song.name)
      .setURL(song.url)
      .setAuthor('Playing now:', bot.user.avatarURL())
      .setDescription(`**Requested by:** ${song.user}\n**Duration:** ${song.formattedDuration}\n\n**Channel:** ${song.info.videoDetails.author.name}\n\n**Ratings:** 👍 ${new Intl.NumberFormat('de-DE').format(song.likes)} **/** ${new Intl.NumberFormat('de-DE').format(song.dislikes)} 👎 **|** ${new Intl.NumberFormat('de-DE').format(song.views)} views`);

    try {
      emb.setThumbnail(song.thumbnail);
    } catch {
      console.error('Thumbnail error.');
    }

    message.channel.send(emb);
  },
};
