const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "invite",
  description: "Invite me to your server!",
  detailedDescription: "Get a link to invite me to your server as well as a link to join the support server.",
  examples: function (pref) {
    return [`${pref}${this.name}`];
    // example: [`${pref}${this.name}`];
  },
  usage: "", // [required] <optional>
  aliases: ['join'],
  permissions: [],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    const infoEmb = new Discord.MessageEmbed()
      .setColor(bot.config.colors.blue)
      .setTitle('Invite me to your server!')
      .setDescription(`[Click Me!](https://discord.com/api/oauth2/authorize?client_id=824635130249084960&permissions=8&scope=bot "If you see this you're a legend! Thanks for inviting me :3")`);

    message.channel.send(infoEmb).then(m => {
      m.channel.send(`**Support server:** https://discord.gg/yCs6pPCtvJ`);
    })
  }
};