const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "ping",
  description: "Pong!",
  detailedDescription: "Pings the bot to see the response time, aka the time it took to respond to your command. Also displays Web Socket ping time.",
  examples: function (pref) {
    return [`${pref}${this.name}`]; 
    // example: [`${pref}${this.name}`];
  },
  usage: "", // [required] <optional>
  aliases: [],
  permissions: [],
  group: "general",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    const initialEmbed = new Discord.MessageEmbed()
      .setColor(bot.config.colors.blue)
      .setDescription("Pinging...");

    message.channel.send(initialEmbed).then(sent => {
      const finalEmbed = new Discord.MessageEmbed()
        .setColor(bot.config.colors.green)
        .setDescription(`Pong! :ping_pong:\n\n:person_running: Response Time: ${sent.createdTimestamp - message.createdTimestamp}ms\n:globe_with_meridians: Web Socket: ${bot.ws.ping}ms`);

      sent.edit(finalEmbed);
    });
  }
};