const Discord = require("discord.js");
const db = require('quick.db');
const ftl = require("findthelyrics");

module.exports = {
  name: "lyrics",
  description: "Get lyrics for the current song!",
  detailedDescription: "Get lyrics for the current song or search for lyrics for a song you want by providing a title.",
  examples: function (pref) {
    return [`${pref}${this.name}`, `${pref}${this.name} Sophia Angeles - Miss You More`];
    // example: [`${pref}${this.name}`];
  },
  usage: "<title>", // [required] <optional>
  aliases: ['ly'],
  permissions: [],
  group: "music",
  cooldown: 10,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    let pfix = db.fetch('guilds').find(i => message.guild.id === i.id).prefix;

    if (args[0]) {
      let q = args.join(' ');

      ftl.find(q, function (err, resp) {
        if (!err) {
          let parts = resp.match(/[\s\S]{1,2000}/g) || [];

          if (parts[0]) {
            bot.f.sendEmb(message.channel, 'blue', `Lyrics found for **${q}**:\n_Provided with the help of [\`normanlol\` on GitHub](https://github.com/normanlol) <3_`)
            parts.forEach(part => {
              bot.f.sendEmb(message.channel, 'blue', part);
            })
          } else bot.f.sendEmb(message.channel, 'red', 'Something went wrong, try again.');
        } else {
          bot.f.sendEmb(message.channel, 'red', `Could not find any lyrics for \`${q}\` !`);
        }
      });
    } else {
      let queue = distube.getQueue(message);
      if (!queue) return bot.f.sendEmb(message.channel, 'red', 'No music is being played!');

      let q = queue.songs[0].name

      ftl.find(q, function (err, resp) {
        if (!err) {
          let parts = resp.match(/[\s\S]{1,2000}/g) || [];

          if (parts[0]) {
            bot.f.sendEmb(message.channel, 'blue', `Lyrics found for **${q}**:\n_Provided with the help of [\`normanlol\` on GitHub](https://github.com/normanlol) <3_`)
            parts.forEach(part => {
              bot.f.sendEmb(message.channel, 'blue', part);
            })
          } else bot.f.sendEmb(message.channel, 'red', 'Something went wrong, try again.');
        } else {
          bot.f.sendEmb(message.channel, 'red', `Could not find any lyrics for \`${q}\` !\n\n_Sometimes the video title can mess up the search, try to manually search for the lyrics like this: \`${pfix}lyrics Artist - Song Name\`_`);
        }
      });
    }
  }
};