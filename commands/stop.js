const Discord = require("discord.js");
const db = require('quick.db');

module.exports = {
  name: "stop",
  description: "Stop the playback!",
  detailedDescription: "Stops the playback and clears the queue. Only DJ's, Admins and you if you're alone with the bot can do this.",
  examples: function (pref) {
    return [`${pref}${this.name}`]; 
    // example: [`${pref}${this.name}`];
  },
  usage: "", // [required] <optional>
  aliases: ["fuckoff", "die", "disconnect", "bye"],
  permissions: [],
  group: "music",
  cooldown: 1,
  args: false,
  ownerOnly: false,
  guildOnly: true,
  execute: async (message, args, bot, distube) => {
    if (!message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in a Voice Channel!");
    let queue = distube.getQueue(message);
    if (!queue) return bot.f.sendEmb(message.channel, 'red', "No music is being played!");
    if (message.guild.me.voice.channel !== message.member.voice.channel) return bot.f.sendEmb(message.channel, 'red', "You're not in the same Voice Channel as me!");

    if (message.member.roles.cache.filter(i => i.id !== message.guild.id).map(i => i.name.toLowerCase()).includes('dj') || message.member.permissions.has('ADMINISTRATOR') || message.guild.me.voice.channel.members.size === 2) {
      message.react('👍');
      if (bot.skips.has(message.guild.id)) {
        bot.skips.get(message.guild.id).skips = 0;
        bot.skips.get(message.guild.id).skippers = [];
      }
      distube.stop(message);
      return;
    } else {
      bot.f.sendEmb(message.channel, 'red', `You need the DJ role in order to do stop the playback!\n_(Having Administrator permissions or being alone with the bot also works)_`);
    }
  }
};