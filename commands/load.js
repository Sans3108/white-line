const Discord = require("discord.js");
const db = require('quick.db');
const fs = require('fs');

module.exports = {
  name: "load",
  description: "Load a command.",
  detailedDescription: "Load a command.",
  examples: function (pref) {
    return [`${pref}${this.name} someNewCommand`];
  },
  usage: "[command file name]",
  aliases: [],
  permissions: [],
  group: "dev",
  cooldown: 1,
  args: true,
  ownerOnly: true,
  guildOnly: false,
  execute: async (message, args, bot, distube) => {
    let cmd = args[0];
    try {
      const newCommand = require(`./${cmd}.js`);
      message.client.commands.set(newCommand.name, newCommand);
      message.channel.send(`Command was loaded!`);
    } catch (error) {
      console.error(error);
      message.channel.send(`There was an error while loading the command: \n\`${error.message}\``);
    }
  }
};
